# README

This Project is still in a early age.

* What can you do with this Dashboard?
- Check earnings (Full list)
- Current Blocks on Mining Pool
- Miner Overview
- tbd...

How to get started?:

## Setup Application

    bundle install
    bundle exec rake db:create
    bundle exec rake db:migrate

## Create a User

    bundle exec rails console
    User.create(email: 'some.user@mail.com', password: 'xxx', password_confirmation: 'xxx')

## Set MONERO_WALLET env var

    export MONERO_WALLET='your wallet'


## Fireup Foreman

    bundle exec foreman start


## Start up Workers

You have to start the workers by hand atm.. Will be changed.

    GlobalInfoFetcherWorker.perform_async
    MinerFetcherWorker,perform_async
    MoneroBlockInfoWorker.perform_async
    Miner.each do |miner|
      MinerInfoFetcherWorker.perform_async(miner.identifier)
    end


That's all for the moment :)