class MinersController < ApplicationController
  def index
    @miners = Miner.all.order(identifier: :asc)#.includes(:miner_infos)
    @miner_infos = MinerInfo.order(created_at: :desc).limit(100)
    @info = GlobalInfo.last
  end
  def show
    @miner = Miner.where(id: params[:id])
  end
end
