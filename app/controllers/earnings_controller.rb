class EarningsController < ApplicationController
  def index
    @total_days = (GlobalInfo.last.created_at.to_date - GlobalInfo.first.created_at.to_date).to_i
    @total_earned = GlobalInfo.last.amt_due - GlobalInfo.first.amt_due

    @earned_1d = @total_earned / @total_days
    @earned_1w = @earned_1d * 7
    @earned_1m = @earned_1d * 30
    @earned_1y = @earned_1d * 365

    @url = "https://min-api.cryptocompare.com/data/price?fsym=XMR&tsyms=CHF,USD,EUR"
    @worth = JSON.parse(URI.parse(@url).read)

    @entries = []
    @earnings = []
    GlobalInfo.all.order(amt_due: :asc, created_at: :asc).uniq {|x| x.amt_due}.each_slice(2).each do |slice|
      next if slice.size < 2
      @entries << {id: slice[0].last_hash, title: "#{(slice[1].amt_due - slice[0].amt_due) / 1000000000000.to_f}", start: Time.at(slice[1].last_hash - 3600), end: Time.at(slice[1].last_hash)}
      @earnings << {id: slice[1].id, earned: (slice[1].amt_due - slice[0].amt_due) / 1000000000000.to_f, due: slice[1].amt_due / 1000000000000.to_f, time: Time.at(slice[1].last_hash)}
    end
    @today = GlobalInfo.where(id: @earnings.collect { |x| x[:id] }, last_hash: Date.today.beginning_of_day.to_i..Date.today.end_of_day.to_i)
                       .order(amt_due: :asc)
    @yesterday = GlobalInfo.where(id: @earnings.collect { |x| x[:id] }, last_hash: Date.yesterday.beginning_of_day.to_i..Date.yesterday.end_of_day.to_i)
                           .order(amt_due: :asc)
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @entries }
    end
  end
end
