class BlocksController < ApplicationController
  def index
    @monero_blocks = MoneroBlock.all.order(unlocked: :asc, ts: :desc)
  end
end
