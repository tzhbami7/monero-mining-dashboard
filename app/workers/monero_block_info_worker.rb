class MoneroBlockInfoWorker
  include Sidekiq::Worker

  def perform(*args)
    url = "https://supportxmr.com/api/pool/blocks/pplns"
    json = JSON.parse(URI.parse(url).read)
    mappings = {
      "ts": 'ts', "hash": "hash_id", "diff": 'diff',
      "shares": 'shares', "height": 'height', "valid": 'b_valid',
      "unlocked": 'unlocked', "pool_type": 'pool_type', "value": 'b_value'
    }
    json.each do |data|
      values = data.inject({}) { |x,k| x[mappings[k[0].to_sym]] = k[1]; x }
      record = MoneroBlock.where(hash_id: data['hash']).first
      record ||= MoneroBlock.new
      record.attributes = values.delete_if { |key| key == nil }
      record.save
    end
    MoneroBlockInfoWorker.perform_in(60.seconds)
  end
end
