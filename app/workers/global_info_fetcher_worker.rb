class GlobalInfoFetcherWorker
  include Sidekiq::Worker

  def perform()
    url = "https://supportxmr.com/api/miner/#{ENV['MONERO_WALLET']}/stats"
    json = JSON.parse(URI.parse(url).read)
    mappings = {
      "hash": 'hashrate', "identifier": "identifier", "lastHash": 'last_hash',
      "totalHashes": 'total_hashes', "validShares": 'valid_shares', "invalidShares": 'invalid_shares',
      "timeout": 'timeout', "amtPaid": 'amt_paid', "amtDue": 'amt_due', "txnCount": 'txn_count'
    }
    values = json.inject({}) { |x,k| x[mappings[k[0].to_sym]] = k[1]; x }
    record = GlobalInfo.new(values)
    unless GlobalInfo.where(last_hash: json['lastHash']).present?
      record.save
      store_metrics(values)
    end
    GlobalInfoFetcherWorker.perform_in(10.seconds)
  end

  def store_metrics(values)
    data = [
      {
        series: 'shares',
        tags:   { identifier: values['identifier'] },
        values: { valid: values['valid_shares'].to_i, invalid: values['invalid_shares'].to_i }
      },
      {
        series: 'hashrate',
        tags:   { identifier: values['identifier'] },
        values: { value: values['hashrate'].to_i },
      }
    ]

    $influxdb.write_points(data, 's')
  end
end
