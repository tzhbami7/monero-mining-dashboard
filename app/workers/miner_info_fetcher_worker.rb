class MinerInfoFetcherWorker
  include Sidekiq::Worker

  def perform(identifer)
    url = "https://supportxmr.com/api/miner/#{ENV['MONERO_WALLET']}/stats/#{identifer}"
    json = JSON.parse(URI.parse(url).read)
    mappings = {
      "hash": 'hashrate', "lts": "lts", "totalHash": 'total_hash',
      "validShares": 'valid_shares', "invalidShares": 'invalid_shares'
    }
    values = json.inject({}) { |x,k| x[mappings[k[0].to_sym]] = k[1]; x }
    record = MinerInfo.new(values.delete_if { |key| key == nil })
    # TODO: typo --> identifer not identifier...
    miner = Miner.where(identifier: json['identifer']).first!
    record.miner_id = miner.id
    unless MinerInfo.where(miner_id: miner.id, lts: json['lts']).present?
      record.save
      store_metrics(values, identifer)
    end
    MinerInfoFetcherWorker.perform_in(10.seconds, identifer)
  end

  def store_metrics(values, identifier)
    data = [
      {
        series: 'shares',
        tags:   { identifier: identifier },
        values: { valid: values['valid_shares'].to_i, invalid: values['invalid_shares'].to_i }
      },
      {
        series: 'hashrate',
        tags:   { identifier: identifier },
        values: { value: values['hashrate'].to_i },
      }
    ]

    $influxdb.write_points(data, 's')
  end
end
