class MinerFetcherWorker
  include Sidekiq::Worker

  def perform(*args)
    url = "https://supportxmr.com/api/miner/#{ENV['MONERO_WALLET']}/identifiers"
    json = JSON.parse(URI.parse(url).read)
    stored = Miner.where(identifier: json)
    (json - stored.collect(&:identifier)).each do |identifier|
      Miner.create(identifier: identifier)
    end
    MinerFetcherWorker.perform_in(10.minutes)
  end
end
