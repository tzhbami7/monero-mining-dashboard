require 'sidekiq/web'
Rails.application.routes.draw do
  devise_for :users
  mount Sidekiq::Web => '/sidekiq'

  resources :miners, only: [:index, :show]
  resources :earnings, only: [:index]
  resources :blocks

  root to: "miners#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
