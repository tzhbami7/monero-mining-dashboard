class CreateMiners < ActiveRecord::Migration[5.1]
  def change
    create_table :miners do |t|
      t.string :identifier
      t.timestamps
      t.index :identifier
    end
  end
end
