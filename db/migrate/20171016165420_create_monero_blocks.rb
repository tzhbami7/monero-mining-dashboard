class CreateMoneroBlocks < ActiveRecord::Migration[5.1]
  def change
    create_table :monero_blocks do |t|
      t.bigint :ts
      t.string :hash_id
      t.bigint :diff
      t.bigint :shares
      t.bigint :height
      t.boolean :b_valid
      t.boolean :unlocked
      t.string :pool_type
      t.bigint :b_value
      t.index :hash_id, unique: true
      t.timestamps
    end
  end
end
