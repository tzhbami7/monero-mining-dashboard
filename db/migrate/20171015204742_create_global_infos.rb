class CreateGlobalInfos < ActiveRecord::Migration[5.1]
  def change
    create_table :global_infos do |t|
      t.bigint :amt_due
      t.bigint :amt_paid
      t.bigint :total_paid
      t.bigint :total_hashes
      t.bigint :hashrate
      t.bigint :last_hash
      t.bigint :txn_count
      t.bigint :valid_shares
      t.bigint :invalid_shares
      t.string :identifier
      t.bigint :timeout

      t.timestamps
    end
  end
end
