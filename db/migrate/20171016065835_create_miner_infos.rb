class CreateMinerInfos < ActiveRecord::Migration[5.1]
  def change
    create_table :miner_infos do |t|
      t.references :miner, index: true
      t.bigint :lts
      t.bigint :hashrate
      t.bigint :total_hash
      t.bigint :valid_shares
      t.bigint :invalid_shares
      t.timestamps
    end
  end
end
