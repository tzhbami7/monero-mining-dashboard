# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171016165420) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "global_infos", force: :cascade do |t|
    t.bigint "amt_due"
    t.bigint "amt_paid"
    t.bigint "total_paid"
    t.bigint "total_hashes"
    t.bigint "hashrate"
    t.bigint "last_hash"
    t.bigint "txn_count"
    t.bigint "valid_shares"
    t.bigint "invalid_shares"
    t.string "identifier"
    t.bigint "timeout"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "miner_infos", force: :cascade do |t|
    t.bigint "miner_id"
    t.bigint "lts"
    t.bigint "hashrate"
    t.bigint "total_hash"
    t.bigint "valid_shares"
    t.bigint "invalid_shares"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["miner_id"], name: "index_miner_infos_on_miner_id"
  end

  create_table "miners", force: :cascade do |t|
    t.string "identifier"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["identifier"], name: "index_miners_on_identifier"
  end

  create_table "monero_blocks", force: :cascade do |t|
    t.bigint "ts"
    t.string "hash_id"
    t.bigint "diff"
    t.bigint "shares"
    t.bigint "height"
    t.boolean "b_valid"
    t.boolean "unlocked"
    t.string "pool_type"
    t.bigint "b_value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hash_id"], name: "index_monero_blocks_on_hash_id", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
